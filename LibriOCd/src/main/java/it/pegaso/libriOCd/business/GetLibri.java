package it.pegaso.libriOCd.business;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.pegaso.libriOCd.model.LibroCd;
import it.pegaso.libroOCd.dao.LibriOcdDao;

/**
 * Servlet implementation class GetLibri
 */
public class GetLibri extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetLibri() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		LibriOcdDao dao = new LibriOcdDao();
		List<LibroCd> result = new ArrayList<LibroCd>();
		
		result = dao.getAll();
		
		request.setAttribute("list", result);
		
		RequestDispatcher rd = request.getRequestDispatcher("showLibri.jsp");
		
		rd.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
