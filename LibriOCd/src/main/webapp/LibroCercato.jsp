<%@page import="it.pegaso.libriOCd.model.LibroCd"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Libro cercato</title>
</head>
<body style="background-color: cyan;">
	
	<% LibroCd lcd = (LibroCd) request.getAttribute("articolo"); 
	
	if(lcd == null){
	%>
		<h1>ERRORE</h1>
		
	<% } else{ %>
	
		<p><%= lcd.toString() %></p>
		
	<% } %>
	

</body>
</html>